#!/bin/sh

set -e

QUEUE=pgsql
TASKS=${QUEUE}tasks
APP=gvapgsql
export TZ="Europe/Berlin"

. "/srv/${APP}/.venv/bin/activate"
cd /srv/${APP}/${APP}
celery -A "${TASKS}" worker -Q "${QUEUE}" -E -l info
