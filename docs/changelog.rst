Changelog
=========

* :release:`0.4.0 <2023-05-08>`
* :support:`-` switch from Pipenv to Poetry

* :release:`0.3.0 <2020-04-05>`
* :support:`-` add Docker setup for lightweight local testing
* :support:`-` update Vagrant setup to libvirt and Debian Buster
* :support:`-` move pgsqltasks to top level to keep the task names when
  using Python 3
* :support:`-` use Pipenv for dependency management

* :release:`0.2.0 <2018-11-21>`
* :feature:`-` fix compatibility with Python 2.7 on Debian Stretch

* :release:`0.1.0 <2015-01-10>`
* :feature:`-` provide functionality to create and delete PostgreSQL users and databases
* :feature:`-` initial project setup
