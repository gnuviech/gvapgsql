==================
Code documentation
==================

gvapgsql is implemented as `Celery`_ app.

.. _Celery: http://www.celeryproject.org/


:py:mod:`pgsqltasks` app
========================

:py:mod:`pgsqltasks.celery`
---------------------------

.. automodule:: pgsqltasks

.. automodule:: pgsqltasks.celery
   :members:


:py:mod:`pgsqltasks.settings`
-----------------------------

.. automodule:: pgsqltasks.settings


:py:mod:`pgsqltasks.tasks`
--------------------------

.. automodule:: pgsqltasks.tasks
   :members:
   :undoc-members:
