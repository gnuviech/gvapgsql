"""
This module defines the Celery_ app for gvapgsql.

.. _Celery: http://www.celeryproject.org/

"""
from celery import Celery

#: The Celery application
app = Celery("pgsqltasks")

app.config_from_object("pgsqltasks.settings", namespace="CELERY")
app.autodiscover_tasks(["pgsqltasks.tasks"], force=True)
