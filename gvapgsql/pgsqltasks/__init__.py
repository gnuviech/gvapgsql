"""
This module contains :py:mod:`pgsqltasks.tasks`.

"""

__version__ = "0.4.0"

from pgsqltasks.celery import app as celery_app

__all__ = ("celery_app",)
