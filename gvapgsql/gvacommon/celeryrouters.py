# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class GvaRouter(object):
    def route_for_task(self, task, args=None, kwargs=None):
        for route in ["ldap", "file", "mysql", "pgsql"]:
            if route in task:
                return {"exchange": route, "exchange_type": "direct", "queue": route}
        return None
